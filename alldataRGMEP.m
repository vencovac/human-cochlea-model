function [x,Gs,Ginv,stiff,ShSp,undamp,bigamma,wm2,Qbm,...
    MEinv,hME,kME,Gme,Sty,GammaMi,Pea,Ve,Wbm,Sow,BETA] = alldataRGMEP(N,gain)


if nargin <2,gain=0.96;end
if nargin <1,N=800;end
% if rebuild_flag==1
%     delete BMDATAME.MAT
%     delete TMDATA.MAT
% end
% 
% %________________________BM data_______________________________
% if exist('BMDATAME.MAT')==2,
% 	load BMDATAME.MAT x N M stiff dampn damp0 ShSp G Gs Ginv Qbm Qs MEinv mME kME hME Gme Sty gammaAir ...
%       Vtc GammaMi Sow Pec Wbm Sow -mat
%     
% else,
% 	disp(' File BMDATAME.MAT does not exist. It will be created!');
%     bmdataME(N); 
%     disp('	Now BMDATAME.MAT does exist');
%     load BMDATAME.MAT x N M stiff dampn damp0 ShSp G Gs Ginv Qbm Qs MEinv mME kME hME Gme Sty gammaAir ...
%       Vtc GammaMi Sow Pec Wbm Sow -mat
% end
% %________________________TM data________________________________
% if exist('TMDATA.MAT')==2,
% 	load TMDATA.MAT bigamma wm2 -mat
% else
%     disp('File TMDATA.MAT does not exist. It will be created!');
%     tmdata(N);    
%     disp('Now TMDATA.MAT does exist.');
%     load TMDATA.MAT bigamma wm2   -mat
% end

[x,N,M,stiff,dampn,damp0,ShSp,G,Gs,Ginv,Qbm,Qs,MEinv,mME,kME,hME,Gme,Sty, ...
      Ve,GammaMi,Sow,Pea,Wbm,BETA] = bmdataMEP(N);
[bigamma, wm2] =  tmdataP(N);

%________________________________________________
%   load LAMBDAS6a.MAT lam -mat
%   if N~=600
%       lamo=lam;lam=[];
%       lam=AdoptLambda(lamo,N);
%   end
  
  % shape undamping to fit the SFOAE data of Shera
  
  load LAMBDAS6a.MAT lam -mat
  if N~=600
      lamo=lam;lam=[];
      lam=AdoptLambda(lamo,N);
  end
%  lam = (lam+1)/2 + 0.07;
%  lam = (lam+1)/3 + 0.43;
  lam = (lam+1)/6 + 0.79;
%   shape undamping to fit the SFOAE data of Shera
  
 %n = [100 200 301 486 700];
%  A = [2 1.5 1.3 1 1];
 %A = [1 1.1 1.05 1 1];
 %nx = 1:800;
 %Ax = interp1(n,A,nx,'spline','extrap');

%   shapeOn = hann(100);
%   lam2 = (tanh(10*(x-0.4))+1)/2; % to attenuate high freq
%   
%   shapeAll =[shapeOn(1:50); ones(N-100,1); shapeOn(51:end)];
  %shapeAll = 1;
  %undamp=gain*damp0.*bigamma.*lam.*shapeAll.*lam2;
  undamp=gain*damp0.*bigamma.*lam;
 % undamp=gain*damp0.*bigamma;
  undamp=undamp(:);
  stiff=stiff(:);

