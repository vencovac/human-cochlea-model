function [Sig]=Inp_sigPureToneTest(F1,A1,t,phi1)

om1 = 2*pi*F1;

AMo=2.14; % reference equivalent to 2e-5*sqrt(2) Pa
AM1 = AMo*10.^(A1/20);

durHW = 10e-3; % duration of ramp
 
Sig=m_hann(t,durHW)*((AM1.*cos(om1*t+phi1)));
%Sig=((AM1.*cos(om1*t+phi1)));
AM=max(abs(Sig));

% else  
%     Sig=0;
%     if t<0.02  
%         Sig=m_hann(t,durHW)*AM1*cos(om1*t+phi1);
%     elseif (t>=0.02)&(t<0.06)
%         Sig=m_hann(t,durHW)*AM1*cos(om1*t+phi1)+...
%         m_hann(t-0.02,durHW)*AM2*cos(om2*t+phi2);
%        elseif (t>=0.06)&(t<0.06+durHW)    
%            Sig=m_hann(t,durHW)*AM1*cos(om1*t+phi1)+...
%                m_hann(0.06+durHW-t,durHW)*AM2*cos(om2*t+phi2);
%        else
%           Sig=AM1*cos(om1*t+phi1);
%        end
%      %
% 

     
end
 
function w=m_hann(t,dur)
    framp = 1/(2*dur);
    if t<=dur
        w = 0.5 * (1 - cos(2*pi*t*framp));  
    else
        w = 1;
    end
end