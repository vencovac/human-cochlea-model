function [x,N,M,stiff,dampn,damp0,ShSp,G,Gs,Ginv,Qbm,Qs,MEinv,mME,kME,hME,Gme,Sty, ...
      Ve,GammaMi,Sow,Pea,Wbm,BETA] = bmdataMEP(N)

 if nargin <1,N=800;end   
  
  x = gaussgrid(N);  
  
 %--------------GREEN'S FUNCTION----------------
  [Gs, G, Sh,Wbm] = greenf(x);
  
 %--------------MASS----------------------------
  M = diag(mass(x));
  
 %--------------STIFFNESS ----------------------
  x = x(:); 		
  dx=[x(1);x(2:N)-x(1:N-1)]; 
  expk = -3.6*log(10); 	
  ko = 2e+3;		  
  k1= ko*exp(expk*x);
  k1=k1(:);
  stiff=[k1(1)-ko; k1(2:N)-k1(1:N-1)]./(expk*dx);stiff=stiff(:); 
 %--------------DAMPING------------------------- 
  ho = 10e-3; 	
  exph = -log(4);	
  dampn = ho*exp(exph*x);
  hbo = 1.1*ho; 
  heo=1.3*dampn(N);
  expho=log(heo/hbo);
  damp0 =hbo*exp(expho*x); damp0=damp0(:); 
  Ce=2*dampn(N);				
  dampn = dampn + Ce*exp((x-1)/0.075); 
  ShSp=sparse(diag(dampn)) + 65*sparse(Sh);
  
  %---------------MiddleEar------------
  
%   mME = 59e-6; % Mass of the oval window [Kg]
%   hME = 20*500*mME; % damping of the ME [Kg/s]
%   kME = (1500*2*pi)^2*mME; % stiffness of the ME [Kg/s^2]
%   mME = 5.9000e-06; 
%   hME = 1.18;
%   kME = 7.3371e+03;
%   mME = 0.1*59e-6; % Mass of the oval window [Kg]
%   hME = 0.05*0.59; %20*500*mME; % damping of the ME [Kg/s]
%   kME = 0.1*(1500*2*pi)^2*59e-6; % stiffness of the ME [Kg/s^2]

  mME = 0.1*59e-6; % Mass of the oval window [Kg]
  hME = 0.1*0.59; %20*500*mME; % damping of the ME [Kg/s]
  kME = 0.2*(1500*2*pi)^2*59e-6; % stiffness of the ME [Kg/s^2]
  
  Gme = 21.4; % mechanical gain of ossicles [-]
  %Sow = 0.0000032; % (m^2)
  BETA = 0.0335; % lenght of the BM used as normalization for the model parameters
  Sow=3.2e-6/BETA^2; % Surface of the oval window relative to BETA
  
  gammaAir = 1.4; % the ratio of specific heats of air [-]
  Sty = 0.49e-4; % effective area of tympanic membrane [m^2]
  GammaMi = 1.4; % lever ratio of incus
  
  %Pec = 1.01295e5; % ambiente pressure in the ear canal [dyn/m^2]
  Pea = 1.42e5;
  %Vtc = 2*10-4; % volume of tympanic cavity [m^3]
  Ve = 0.5e-6; % volume of tympanic cavity [m^3]
  
  %-------------TIME DOMAIN--------------------
  Qbm=G(1,:);
  Gs = Gs(:);
  Qs=Gs(1); 
  G1G = Gs*Qbm;
  
  MEinv = 1/(mME + Sow*Qs/Wbm(1));
 
  Ginv = inv(G+M-MEinv*G1G*Sow/Wbm(1)); 
 
  
  plotflag = 0;
  plotflag = 0; % plot flag for ploting of figures with parameters
  if plotflag==1
    figure(1)
    subplot(221), 
    plot(x, diag(M)/BETA,'k')
	title('(A) Mass {\itm}({\itx})', 'Interpreter', 'tex'),
    ylabel('Kg/m')
    set(gca,'TickLength',[0.025 0])
    subplot(222),
	semilogy(x, stiff/BETA, 'k-'),
	title('(B) Stiffness {\itk}({\itx})')
    ylabel('Kg/(m s^2)')
	ylim([10 1e5])
    set(gca,...
        'ytick',[1e1 1e3 1e5],...
        'yticklabel',{'1e1','1e3','1e5'},...
        'TickLength',[0.025 0])
    
    
    subplot(223),
	plot(x, dampn/BETA, 'k-'),
	title('(C) Positional viscosity {\ith}({\itx})')
    ylabel('Kg/(m s)')
	xlabel('Fractional distance from stapes');
    set(gca,'TickLength',[0.025 0])
    
    load Shpar_s.mat s;
    subplot(224),
	plot(x, 65*s*BETA, 'k-'),
	title('       (D) Shearing visc. {\its}({\itx})')
    ylabel('Kg m/s')
	xlabel('Fractional distance from stapes');
    ylim([0 1.2e-9])
    set(gca,'TickLength',[0.025 0])  
      
      
  end