function [bigamma, wm2] =  tmdataP(N)
 
if nargin <1,N=800;end  
 x = gaussgrid(N);

 base=2.4531;   
 alpha=-6.36;  
 Fo=2.11e+004; 

 wm=2*pi*Fo*base.^(alpha*(x+0.05));
 wm2=wm.*wm;  % equivalent to K/M along the TM 
 bigamma=2*pi*Fo*base.^(0.5*alpha*(x+0.05));
 bigamma=bigamma(:)./0.95;
 wm2=wm2(:);% equivalent to H/M along the TM 

 BETA = 33.5e-3;
 
 
 plotflag = 0;
 if plotflag==1
    close all
    load BMDATAME.MAT x damp0;
    figure(1)
    subplot(221), 
    plot(x, bigamma,'k',x,2*wm,'k:')
	title('      (A) TM damping \gamma_{TM}({\itx})', 'Interpreter', 'tex'),
    ylabel('rad/s')
    set(gca,'TickLength',[0.025 0])  
    subplot(222),
    semilogy(x, wm/(2*pi), 'k-'),
    xlabel('Fractional distance from stapes');
	title('(B) TM resonance frequency')
    ylabel('Hz')
	set(gca,'TickLength',[0.025 0])  
    subplot(223),
    plot(x, damp0/(BETA), 'k-'),
	title('(C) Parameter {\ith}_0({\itx})')
    ylabel('Kg/(m s)')
    xlabel('Fractional distance from stapes');
    set(gca,'TickLength',[0.025 0])  
    
    Y = -0.2:0.0001:0.3;
    [Z]=nonlin(Y)
    subplot(224),
    plot(Y,Z, 'k-'),
	title('(D) Sigmoidal function {\itS}({\ity})')
   % text(0.2,0.102,'S[{\ita}\eta]','fontname','serif','fontweight','bold')
    set(gca,'TickLength',[0.025 0])  

%plot(xSig,0*xSig,'linewidth',LW,'color',[.5 .5 .5]);
%plot(0*xSig,xSig,'linewidth',LW,'color',[.5 .5 .5]);
    hold on;
    plot(Y,Y,'color',[.5 .5 .5]);

set(gca,...
    'xlim',[-0.2 0.25],...
    'ylim',[-0.05 0.09],...
    'box','on',...
    'xtick',[-0.1 0 0.1 0.2],...
    'ytick',[-0.04 -0.02 0 0.02 0.04 0.06 0.08],...
    'yticklabel',{'','','','0.02','0.04','0.06','0.08'}...
   );

%text(-0.18,-0.08, 'Fractional distance from stapes');
set(gca,'xaxislocation','origin','yaxislocation','origin');
%text(0.01,-0.02,'-0.02','fontsize',iFontSize,'fontname',strFontName,'color',[.15 .15 .15]);
%text(0.01,-0.04,'-0.04','fontsize',iFontSize,'fontname',strFontName,'color',[.15 .15 .15]);

%text(0.25,0.01,'{\ita}\eta','fontsize',iFontSize+2,'fontname','serif','color',[0 0 0],'interpreter','tex');
%text(0.001,0.1,'S[{\ita}\eta]','fontsize',iFontSize+2,'fontname','serif','color',[0 0 0]);
text(0.18,0.01,'{\ity}','fontname','serif');%'fontsize',iFontSize+2,'fontname',strFontName,'color',[0 0 0]);
%text(0.001,0.1,'S[\eta]');%'fontsize',iFontSize+2,'fontname',strFontName,'color',[0 0 0]);
      
 end