function lam=AdoptLambda(lamo,N)

 if nargin<2,N=800;end
 if nargin<1,
    load LAMBDAS6a.MAT lam
    lamo=lam;lam=[];
 end
 No=length(lamo);
 xo = gaussgrid(No); 
 x = gaussgrid(N); 

 lam=interpol(xo,lamo,x);
 lam=lam(:);
 
 if nargout<1
   figure(1)
    clf
     plot(xo,lamo,'k',x,lam,'r')
     lam=[];
 end