function [BMdisp, OWvel, Pd, Sig, tim, Pe, OWvelFT, PdFT, PeFT, BMdispFT]=CochleaSetEverythingUp_meTal(A1o,F1o,Tdur,Ma,gain,phi1o,visual)

if nargin < 1,  A1o=50; end
if nargin < 2,  F1o=2000; end
if nargin < 3,  Tdur=0.1;end
if nargin < 4,  Ma=9e-2; end
if nargin < 5,  gain=0.85; end
if nargin < 6,  phi1o=0; end
if nargin < 7,  visual=1; end

global Gs Ginv DampSp stiff bigamma  wm2 undamp N ...
        MEinv hME kME Gme Sty GammaMi Pea Ve Qbm Wbm Sow BETA
    
global A1 F1 phi1

ResetAll; %reset global

%Sf=S;
A1=A1o;F1=F1o;phi1=phi1o;

h=5e-6;   %time step
fs=1/h;   %sampling frequency
I=sqrt(-1);
im1=-I*2*pi*F1;
ts = 15e-3; % onset time set according to ramp duration
%________________________MODEL PARAMETERS_____________________________________%
 N=800;
 
 [x,Gs,Ginv,stiff,DampSp,undamp,bigamma,wm2,Qbm,...
     MEinv,hME,kME,Gme,Sty,GammaMi,Pea,Ve,Wbm,Sow,BETA] = alldataRGMEP(N,gain);
  

Nsamp = round(Tdur*fs);
if visual==1
    tx = 0:1/fs:(Nsamp-1)/fs;
    Sig = zeros(size(tx));
    for k=1:length(tx)
        [Sig(k)]=Inp_sigPureToneTest(F1,A1,tx(k),phi1);
    end
    Ma1=max(abs(Sig))*1.1;
 %   Lp=length(t1);
%________________________Graphics_________________________________
 y=zeros(size(x));
 
 H_FIG = figure('Name','HUMAN COCHLEA MODEL','DoubleBuffer','on','NumberTitle','off');
   H_BM=axes('Position', [.072 .1 .9 .4], 'Box', 'on','XLim',[x(1),x(N)],'YLim',[-Ma,Ma]);
      h_tit1 = text(-1, 0, 'BM displacement');
      set(H_BM, 'Title', h_tit1);
      h_L1 = line('XData',x,'YData',zeros(N,1),'Color', 'r');
           xlabel('Distance from stapes [cm]');
         
   H_SIG =	axes('Box', 'on','Position',[0.072,0.675,0.9,0.25]);
   set(H_SIG,'Xlim', [0, tx(Nsamp)], 'Ylim', [-Ma1, Ma1]);
   h_tit2 = text(-1, 0, ['Input signal-->F1:=',num2str(F1),' [Hz]'],'Tag','h_tit2');
   h_xlbl2 = text(-1,0, 'Time [sec]', 'Tag','h_xlbl2');
   h_ylbl2 = text(-1,0, 'Amplitude', 'Tag', 'h_ylbl2');
   set(H_SIG, 'Title', h_tit2,'XLabel', h_xlbl2, 'YLabel',h_ylbl2);
   h_L2 = line('XData',tx,'YData',Sig,'Erasemode','none','Color', 'b');
   h_L0=line('Xdata', [tx(1), tx(1)], 'Ydata', [-Ma1, Ma1]);   
   set(h_L0, 'Color', 'r');
   drawnow;
end 
 %_________________________RUNGE KUTTA____________________________________%
 n_c=1;             %statistic for graphics 
 t0=0;              %start time
 y0=zeros(4*N+4,1);   %initial conditions
 y=y0(:);
 t=t0;
 neq = length(y);
 %------------------------------------------- 
 pow = 1/5;
 A = [1/5; 3/10; 4/5; 8/9; 1; 1];
 B = [
    1/5         3/40    44/45   19372/6561      9017/3168       35/384
    0           9/40    -56/15  -25360/2187     -355/33         0
    0           0       32/9    64448/6561      46732/5247      500/1113
    0           0       0       -212/729        49/176          125/192
    0           0       0       0               -5103/18656     -2187/6784
    0           0       0       0               0               11/84
    ];

F = zeros(neq,6);


hA = h * A;
hB = h * B;
%____________________Main routine___________________________________%
Y1FT=zeros(N,1);
Y2FT=zeros(N,1);
YDP1FT=zeros(N,1);
YDP2FT=zeros(N,1);
count=0;

BMdisp = zeros(Nsamp,N);
OWvel = zeros(Nsamp,1);
% Pd = zeros(Lp,1);
tim = zeros(Nsamp,1);
Pd = zeros(Nsamp,1);
Pe = zeros(Nsamp,1);
%OWvel = 0;

OWvelFT = 0;
PeFT = 0;
BMdispFT = zeros(N,1);
PdFT = 0;
n_cFT = 0;

while count<Nsamp   
                             
       F(:,1) = activeC(t,y);
       F(:,2) = activeC(t + hA(1), y + F*hB(:,1));
       F(:,3) = activeC(t + hA(2), y + F*hB(:,2));
       F(:,4) = activeC(t + hA(3), y + F*hB(:,3));
       F(:,5) = activeC(t + hA(4), y + F*hB(:,4));
       F(:,6) = activeC(t + hA(5), y + F*hB(:,5));
       
       %II=Inp_sigOneToneS(F1,A1,t + hA(5),Sf,phi1);
       %dy=F*hB(:,6);
%        oae(count+1)=Qs*II+Qbm*dy(N+1:2*N)/h;
       tim(count+1)=t + hA(5);
        
       t = t + hA(6);
       y = y + F*hB(:,6);
       
              
       if n_c==8&&visual==1 %graphics
           set(h_L1,'Ydata',y(1:N));
           set(h_L0, 'Xdata', [t, t], ...
             'Color', 'r');    
           n_c=0;
           drawnow; 
       end
       n_c=n_c+1;
       count=count+1;
        
       BMdisp(count,:) = y(1:N); % get BM displacement
       OWvel(count) = y(4*N+2); % get OW velocity
       Pe(count) = F(4*N+4,6);
       Pd(count)= F(4*N+3,6); % get vestibualr pressure P(0,t)
       %OWvel = OWvel*0.0335;
       if t>ts %DFT
           OWvelFT = OWvelFT + y(4*N+2)*exp(im1*t); % phasor of OWvel           
           PdFT = PdFT + F(4*N+3,6)*exp(im1*t); % phasor of Pd
           PeFT = PeFT + F(4*N+4,6)*exp(im1*t); % phasor of Pe
           BMdispFT=BMdispFT+y(1:N)*exp(im1*t); % phasor of BMdisp
           n_cFT=n_cFT+1;   
       end
%                 
end
if visual==1,close(H_FIG); end;

OWvelFT=(2*OWvelFT)/(n_cFT); % 
PdFT=(2*PdFT)/(n_cFT);
PeFT=(2*PeFT)/(n_cFT);
BMdispFT = (2*BMdispFT)/(n_cFT);
% if nargout<1
%     Y1FT=[];Y2FT=[];YDP1FT=[];YDP2FT=[];
% end
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------------------UTILITY----------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%-----------------------------------------------------------%%     
function ResetAll

 global  Gs Ginv DampSp stiff bigamma  wm2 undamp N ...
        MEinv hME kME Gme Sty GammaMi Pea Ve Qbm Wbm Sow BETA
 global  Sf A1 A2 F1 F2 phi1 phi2
 
 Gs=[]; Ginv=[]; DampSp=[]; stiff=[]; bigamma=[]; wm2=[];
 MEinv=[]; hME=[]; kME=[]; Gme=[]; gammaAir=[]; Sty=[]; GammaMi=[];
 Pec = []; Vtc=[]; Qbm=[]; Wbm=[]; Sow=[]; undamp=[]; N=[];
 Sf=[]; A1=[]; A2=[];  F1=[]; F2=[]; phi1=[]; phi2=[]; BETA=[];
 
%---------------------------------------
function dXdV = passiveC(t,XV)

  global BMinput Ginv DampSp  stiff  N   
  global om om2 fac 
  
   inp=-tanh(t*fac)*om2*cos(om*t);
   X=XV(1:N);
   V=XV(N+1:2*N);

   dV = -BMinput*inp-Ginv*(stiff.*X + DampSp*V); 

   dXdV=[V
          dV];

%---------------------------------------
 function dXdVdYdW = activeC(t,XVYW)

global Gs Ginv DampSp stiff bigamma  wm2 undamp N ...
        MEinv hME kME Gme Sty GammaMi Qbm Wbm Sow BETA Pea Ve
    
global A1 F1 phi1
 
   
   inp = Inp_sigPureToneTest(F1,A1,t,phi1);

    X=XVYW(1:N);
    V=XVYW(N+1:2*N);
    Y=XVYW(2*N+1:3*N);
    W=XVYW(3*N+1:4*N);
    Xme = XVYW(4*N+1);
    Vme = XVYW(4*N+2);

    Ycut=nonlin(Y);
%     Ycut = Y;
   %dV = -1*Ginv*(stiff.*X + DampSp*V + undamp.*Ycut - Gs*hMEn*Vme - Gs*kMEn*Xme + Gs*GMEn*inp);
   %dVme = -hMEn*Vme - kMEn*Xme - Qbmn*dV + GMEn*inp;
   dV = -1*Ginv*(stiff.*X + DampSp*V + undamp.*Ycut - Gs*MEinv*hME*Vme - Gs*MEinv*kME*Xme + Gs*MEinv*Sow*Gme*inp*BETA);
   dVme = -1*MEinv*( hME*Vme + kME*Xme + Sow*Qbm*dV/Wbm(1) - Sow*Gme*inp*BETA);
    
   Pe = inp - Pea*Sty*GammaMi/Ve*Xme*BETA;
    
    Pd0 = -Qbm*dV/Wbm(1) - Gs(1)*dVme/Wbm(1); % vestibular pressure

    dXdVdYdW=[ V                 % 1:N
               dV                % N+1:2N
                W                % 2N+1:3N
                -bigamma.*W - wm2.*Y - dV % 3N+1:4N
                  Vme % 4N+1
                   dVme %4N+2
                    Pd0 %4N+3
                    Pe]; %4N+4
                

 

 
